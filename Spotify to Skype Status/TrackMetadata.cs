﻿using System;
using System.Text;
using System.Runtime.InteropServices;
using ProcessInfo;

namespace Metadata
{
    internal class DLL_Methods
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        internal static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        internal static extern int GetWindowTextLength(IntPtr hWnd);
    }


    class TrackMetadata
    {
        public ProcessInformation PSI = null;
        public IntPtr hWnd = IntPtr.Zero;
        public int processid = 0;

        public TrackMetadata()
        {
            PSI = new ProcessInformation();
            hWnd = PSI.getSpotify();
            processid = PSI.getProcessId(hWnd);
        }


        public string GetCurrentTrack()
        {
            int length = DLL_Methods.GetWindowTextLength(hWnd);
            StringBuilder sb = new StringBuilder(length + 1);
            DLL_Methods.GetWindowText(hWnd, sb, sb.Capacity);

            return sb.ToString();
        }

        public string[] getCurrentTrackInfo()
        {
            string[] strArray = null;
            string currentTrack = null;
            currentTrack = GetCurrentTrack();

            if (!string.IsNullOrEmpty(currentTrack))
            {
                if (currentTrack != "Spotify")
                {
                    strArray = currentTrack.Split('-');
                }
                else
                {
                    string[] iString = {"Spotify", "Spotify"};
                    strArray = iString;
                }
            }
            else
                return null;

            return strArray;
        }

        public string getTrack()
        {
            if (getCurrentTrackInfo() == null || getCurrentTrackInfo().Length == 0)
                return null;
            else
                return getCurrentTrackInfo()[1].Trim();

        }

        public string getArtist()
        {
            if (getCurrentTrackInfo() == null || getCurrentTrackInfo().Length == 0)
                return null;
            else
                return getCurrentTrackInfo()[0].Trim();
        }

    }

}