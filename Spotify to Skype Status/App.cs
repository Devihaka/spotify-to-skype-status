﻿using Metadata;
using ProcessInfo;
using SKYPE4COMLib;
using System;
using System.Timers;

namespace Spotify_to_Skype_Status
{
    public class App
    {

        private static Timer _timer = new Timer();

        [STAThread]
        public static void Main()
        {
            _timer.Interval = 1000;
            _timer.Elapsed += TickTock;
            _timer.Start();
            System.Windows.Forms.Application.Run();
        }

        private static void TickTock(object sender, ElapsedEventArgs e)
        {
            var skype = new Skype();
            skype.Attach(5, true);
            TrackMetadata _trackMetadata = new TrackMetadata();
            ProcessInformation _processInfo = new ProcessInformation();

            string _track = _trackMetadata.getTrack();
            string _artist = _trackMetadata.getArtist();
            if (_processInfo.isAvailable())
            {
                if (_track != "Spotify")
                {
                    skype.CurrentUserProfile.MoodText = "(Spotify) " + _track + " - " + _artist;
                }
                else
                {
                    skype.CurrentUserProfile.MoodText = "";
                }

            }
            else
            {
                skype.CurrentUserProfile.MoodText = "";
            }
        }
    }
}
